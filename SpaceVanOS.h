//
// ---------------------------------------------------------------------
//  SpaceVan-OS - To the darkest reaches of deep space - in a Caravan!
// ---------------------------------------------------------------------
//
//      Space Simulator Arduino Controller Node (Code Template)
//
// ---------------------------------------------------------------------
//
//       Created for Connected Community HackerSpace Melbourne
//            [http://www.hackmelbourne.org.au/] for the:
//        CCHS Viscountess Spaceship Simulator (in a caravan!)
//
// ---------------------------------------------------------------------
//
//   Author(s):  Ben Horan <benh@geeksforhire.com.au>
//               John Spencer <insert email here>
//
//   License:    Copyright (c) 2014, Ben Horan & John Spencer
//               Released under GNU Affero General Public License v3
//               [https://gnu.org/licenses/agpl.txt]
//
//  Platform:    This code, and its dependancy libraries have been
//               tested on the FreeTronics EtherMega rev.3 (which
//               includes the Microchip 24AA02E48 EEPROM with unique
//               MAC address). It uses several large libraries for
//               specialist networking and other communications tasks
//               which will not compile into 8/16/32kbit program memory
//               Arduino processors (the MEGA just gives us heaps of 
//               GPIO's and tonnes of memory - who could resist??).
//
//  Version History:    v0.2 beta (Current Version)
//                           - Added logic for obtaining MAC address using
//                             EthernetMacDetect library, DHCP leasing of
//                             IP configuration etc including automatic reconnect
//                             attempts in the event of temporary network problems.
//                           - Created helper functions to handle initialisation
//                             and copying of byte, char and uint8 arrays (as
//                             well as conversion of IPAddress structures to
//                             other variable formats.
//
//  Version History:    v0.1 beta 
//                           - Initial Release
//

#ifndef SpaceVanOS_h
#define SpaceVanOS_h

#include <Arduino.h>
#include <Client.h>
#include <string.h>

#include "libs/EthernetMacDetect.h"

#include "libs/RestClient.h"

#include "libs/PubSubClient.h"

#include "libs/JsonArray.h"
#include "libs/JsonHashTable.h"
#include "libs/JsonObjectBase.h"
#include "libs/JsonParser.h"

#include "libs/Scheduler.h"

#define DEBUG
#include "libs/debug.h"

#define MAC_ADDRESS_LENGTH 6
#define IPV4_ADDRESS_LENGTH 4


typedef enum _ConnectNetworkServicesResult_t {
    Successful = 0,
    Error_AutoMACConfigurationFailed = 1,
    Error_DHCPConfigurationFailed = 2,
    Error_MQTTServerConnectionFailed = 3,
    Error_WebServerConnectionFailed = 4    
} ConnectNetworkServicesResult_t;

typedef enum _MqttClientPresence_t {
    ClientOffline = 0,
    ClientOnline = 1,
    HeartbeatPing = 2   
} MqttClientPresence_t;

typedef struct NetworkServicesInfo {    
    uint8_t* ethernetMacAddress;
    uint8_t* localIP;
    uint8_t* subnetMask;
    uint8_t* gatewayIP;
    uint8_t* dnsServerIP;
    uint8_t* mqttServerIP;
    uint16_t mqttServerPort;
    uint8_t* webServerIP;
    uint16_t webServerPort;
    boolean  macAddressAutoDetected;
    boolean  dhcpIpConfigured;
    boolean  mqttServerConnected;
    boolean  webServerConnected;  
} NetworkServicesInfo;

typedef void (*MqttIncomingCommandCallback)(char*, char*);
typedef void (*MqttIncomingPresenceCallback)(char*, MqttClientPresence_t);
    
class SpaceVanOS {

public:
    SpaceVanOS();
    void begin(const char* clientName, const char* clientVersion, uint32_t options);
    void begin(const char* clientName, const char* clientVersion);    
    void loop(void);
    void end(void);
    ConnectNetworkServicesResult_t connectNetworkServices(uint8_t* localIP, uint8_t* subnetMask, uint8_t* gatewayIP, uint8_t* dnsServerIP, uint8_t* macAddress, MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback, uint8_t connectionAttempts, uint16_t const reconnectDelay);
    ConnectNetworkServicesResult_t connectNetworkServices(uint8_t* macAddress, MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback, uint8_t connectionAttempts, uint16_t const reconnectDelay);
    ConnectNetworkServicesResult_t connectNetworkServices(MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback, uint8_t connectionAttempts, uint16_t const reconnectDelay);
    ConnectNetworkServicesResult_t connectNetworkServices(MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback);
    char* getClientName(void);
    char* getClientVersion(void);
    NetworkServicesInfo getNetworkServicesInfo(void);

private:    
    void copyCharArray(char* dest, int length, char const* source);
    void copyByteArray(byte* dest, int length, byte const* source);
    void copyUint8Array(uint8_t* dest, int length, uint8_t const* source);
    char* initCharArray(int length, char const initChar);
    byte* initByteArray(int length, byte const initByte);
    uint8_t* initUint8Array(int length, uint8_t const initUint8);
    uint8_t* ipAddressToUint8Array(IPAddress ipAddressIn);
    char* ipAddressUint8ArrayToString(uint8_t const* ipAddressArrIn);
    char* ipAddressToString(IPAddress ipAddressIn);
    char* macAddressUint8ArrayToString(uint8_t const* macAddressIn);

    EthernetClient _ethernetClient;
    //PubSubClient _mqttClient;
    //Scheduler _scheduler;
    NetworkServicesInfo _networkServices;
    MqttIncomingCommandCallback _mqttIncomingCommandCallback;
    MqttIncomingPresenceCallback _mqttIncomingPresenceCallback;    
    
    char* _clientName;
    char* _clientVersion;
    char* _mqttPresenceTopic;
    char* _mqttStatusTopic;
    char* _mqttCommandsTopic;

};

#endif