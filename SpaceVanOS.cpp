//
// ---------------------------------------------------------------------
//  SpaceVan-OS - To the darkest reaches of deep space - in a Caravan!
// ---------------------------------------------------------------------
//
//      Space Simulator Arduino Controller Node (Code Template)
//
// ---------------------------------------------------------------------
//
//       Created for Connected Community HackerSpace Melbourne
//            [http://www.hackmelbourne.org.au/] for the:
//        CCHS Viscountess Spaceship Simulator (in a caravan!)
//
// ---------------------------------------------------------------------
//
//   Author(s):  Ben Horan <benh@geeksforhire.com.au>
//               John Spencer <insert email here>
//
//   License:    Copyright (c) 2014, Ben Horan & John Spencer
//               Released under GNU Affero General Public License v3
//               [https://gnu.org/licenses/agpl.txt]
//
//  Platform:    This code, and its dependancy libraries have been
//               tested on the FreeTronics EtherMega rev.3 (which
//               includes the Microchip 24AA02E48 EEPROM with unique
//               MAC address). It uses several large libraries for
//               specialist networking and other communications tasks
//               which will not compile into 8/16/32kbit program memory
//               Arduino processors (the MEGA just gives us heaps of 
//               GPIO's and tonnes of memory - who could resist??).
//
//  Version History:    v0.2 beta (Current Version)
//                           - Added logic for obtaining MAC address using
//                             EthernetMacDetect library, DHCP leasing of
//                             IP configuration etc including automatic reconnect
//                             attempts in the event of temporary network problems.
//                           - Created helper functions to handle initialisation
//                             and copying of byte, char and uint8 arrays (as
//                             well as conversion of IPAddress structures to
//                             other variable formats.
//
//  Version History:    v0.1 beta 
//                           - Initial Release
//

#include "SpaceVanOS.h"

// CONSTRUCTOR

SpaceVanOS::SpaceVanOS(void) {
    //_mqttClient = PubSubClient();
    //_scheduler = Scheduler().getInstance();
    _ethernetClient = EthernetClient();
    
    // Initialise NetworkServices info with zero'd default values
    _networkServices = NetworkServicesInfo();
    _networkServices.ethernetMacAddress = initUint8Array(MAC_ADDRESS_LENGTH, 0x00);
    _networkServices.localIP = initUint8Array(IPV4_ADDRESS_LENGTH, 0);
    _networkServices.subnetMask = initUint8Array(IPV4_ADDRESS_LENGTH, 0);
    _networkServices.gatewayIP = initUint8Array(IPV4_ADDRESS_LENGTH, 0);
    _networkServices.dnsServerIP = initUint8Array(IPV4_ADDRESS_LENGTH, 0);
    _networkServices.mqttServerIP = initUint8Array(IPV4_ADDRESS_LENGTH, 0);
    _networkServices.webServerIP = initUint8Array(IPV4_ADDRESS_LENGTH, 0); 
    _networkServices.mqttServerPort = 1883;
    _networkServices.webServerPort = 80;
    _networkServices.macAddressAutoDetected = false;
    _networkServices.dhcpIpConfigured = false;
    _networkServices.mqttServerConnected = false;
    _networkServices.webServerConnected = false;
    
}

// PUBLIC METHODS

void SpaceVanOS::begin(const char* clientName, const char* clientVersion, uint32_t options) {

    // Define templates for MQTT topics that this client will subscribe/publish to 
    // "%s" placeholder will be replaced with clientName using sprintf below...

    char* presenceTopicTemplate = "spaceship/presence/%s";
    char* statusTopicTemplate = "spaceship/%s/values/%%PROPERTY%%";
    char* commandsTopicTemplate = "spaceship/%s/commands/#";
    
    // Allocate memory for our private variables to store client info
    _clientName = new char[(strlen(clientName)+1)];
    _clientVersion = new char[(strlen(clientVersion)+1)];    

    // Create a copy of client name/version to persist for life of class object

    strlcpy(_clientName, clientName, sizeof(_clientName));
    strlcpy(_clientVersion, clientVersion, sizeof(_clientVersion));
    
    // Allocate memory for new char array to hold MQTT topics in our private variables. Calculate the 
    // length by adding clientName and template string lengths together, subtract 2 bytes (for the %s placeholder
    // in template string), then add byte 1 (for the \0 string terminator)

    _mqttPresenceTopic = new char[((strlen(clientName)+strlen(presenceTopicTemplate))-1)]; 
    _mqttStatusTopic = new char[((strlen(clientName)+strlen(statusTopicTemplate))-1)];
    _mqttCommandsTopic = new char[((strlen(clientName)+strlen(commandsTopicTemplate))-1)];
    
    // Construct MQTT topic names, inserting clientName where appropriate

    snprintf(_mqttPresenceTopic, sizeof(_mqttPresenceTopic), presenceTopicTemplate, clientName);
    snprintf(_mqttStatusTopic, sizeof(_mqttStatusTopic), statusTopicTemplate, clientName);
    snprintf(_mqttCommandsTopic, sizeof(_mqttCommandsTopic), commandsTopicTemplate, clientName);

}

void SpaceVanOS::begin(const char* clientName, const char* clientVersion) {

  begin(clientName, clientVersion, 0);

}

void SpaceVanOS::loop(void) {
    
    //_scheduler.update();

}

void SpaceVanOS::end(void) {
    //
}

ConnectNetworkServicesResult_t SpaceVanOS::connectNetworkServices(uint8_t* localIP, uint8_t* subnetMask, uint8_t* gatewayIP, uint8_t* dnsServerIP, uint8_t* macAddress, MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback, uint8_t connectionAttempts, uint16_t const reconnectDelay) {

    // Attempt to obtain network settings and connect to LAN services
    //
    // We'll use the provided MAC address for the local ethernet client,
    // and a specified IP address - either passed from the main program 
    // as a static IP, or obtained by DHCP in one of the other overloaded 
    // versions of this method call to establish connections to the game
    // server services required for communications and control.
    //
    // We'll retry connecting for specified attempts after the defined
    // reconnectDelay (in milliseconds).

    _mqttIncomingCommandCallback = commandCallback;
    _mqttIncomingPresenceCallback = presenceCallback;

    copyUint8Array(_networkServices.ethernetMacAddress, MAC_ADDRESS_LENGTH, macAddress);    
    copyUint8Array(_networkServices.localIP, IPV4_ADDRESS_LENGTH, localIP);
    copyUint8Array(_networkServices.subnetMask, IPV4_ADDRESS_LENGTH, subnetMask);
    copyUint8Array(_networkServices.gatewayIP, IPV4_ADDRESS_LENGTH, gatewayIP);
    copyUint8Array(_networkServices.dnsServerIP, IPV4_ADDRESS_LENGTH, dnsServerIP);
    
    return Successful;

}

ConnectNetworkServicesResult_t SpaceVanOS::connectNetworkServices(uint8_t* macAddress, MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback, uint8_t connectionAttempts, uint16_t const reconnectDelay) {

    // Attempt to obtain network settings and connect to LAN services
    //
    // We'll be using the supplied MAC address for the local ethernet
    // client - either provided by the main program, or obtained 
    // automatically using the EthernetMacDetect library via one of the
    // other overloaded versions of this method.
    //
    // We'll be attempting to obtain the local IP address via DHCP as it
    // hasn't been supplied by the calling program.
    
    // Attempt to initialise ethernet client and obtain localIP via DHCP.
    
    uint8_t connectionTries = 0;
    uint8_t dhcpResult;
    
    while (connectionTries < connectionAttempts) {
        
        dhcpResult = Ethernet.begin(macAddress);
        
        if (dhcpResult == 1) {  // A result from Ethernet.begin() of 1 indicates a successful DHCP lease obtained            
            _networkServices.dhcpIpConfigured = true;
            
            // Pull the client IP details from the Ethernet object now that we've successfully obtained them from a DHCP server
            // (we need to create a fresh copy of the data returned from the Ethernet methods in memory to pass to our next method)
            uint8_t localIP[IPV4_ADDRESS_LENGTH];
            copyUint8Array(localIP, IPV4_ADDRESS_LENGTH, ipAddressToUint8Array(Ethernet.localIP()));
            uint8_t subnetMask[IPV4_ADDRESS_LENGTH];
            copyUint8Array(subnetMask, IPV4_ADDRESS_LENGTH, ipAddressToUint8Array(Ethernet.subnetMask()));
            uint8_t gatewayIP[IPV4_ADDRESS_LENGTH];
            copyUint8Array(gatewayIP, IPV4_ADDRESS_LENGTH, ipAddressToUint8Array(Ethernet.gatewayIP()));
            uint8_t dnsServerIP[IPV4_ADDRESS_LENGTH];
            copyUint8Array(dnsServerIP, IPV4_ADDRESS_LENGTH, ipAddressToUint8Array(Ethernet.dnsServerIP()));
                        
            // Pass the info we've obtained from DHCP to the higher overload of this method to attempt to establish connections to network services
            return connectNetworkServices(localIP, subnetMask, gatewayIP, dnsServerIP, macAddress, commandCallback, presenceCallback, connectionAttempts, reconnectDelay);                        
        } else {
            // A dhcpResult other than 1 means we hit a snag. Pause for the reconnectDelay period (milliseconds), then loop around to try again...  
            connectionAttempts--;            
            if (reconnectDelay) {
                delay(reconnectDelay);    
            }                        
        }
    }
    
    // We've failed after to obtain an IP address via DHCP after maximum retries - return an error to caller
    
    return Error_DHCPConfigurationFailed;
    
}

ConnectNetworkServicesResult_t SpaceVanOS::connectNetworkServices(MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback, uint8_t connectionAttempts, uint16_t const reconnectDelay) {

    // Attempt to obtain network settings and connect to LAN services
    //
    // Assume that MAC can be obtained automatically by probing I2C EEPROM with embedded MAC address, and that
    // the local IP address will be obtained by DHCP. 

    // Attempt to detect MAC address using EthernetMacDetect library
    
    uint8_t ethernetMacAddress[MAC_ADDRESS_LENGTH];
    boolean macAddressResult;
    
    macAddressResult = EthernetMacDetect.getMacAddressFromEeprom(ethernetMacAddress);
    
    if (macAddressResult == true) {
        
        // As we've successfully obtained a MAC address that is unique to this
        // microcontroller board, pass this to our higher overloaded version of
        // this method for further initialisation processing...

        _networkServices.macAddressAutoDetected = true;
        return connectNetworkServices(ethernetMacAddress, commandCallback, presenceCallback, connectionAttempts, reconnectDelay);
    
    } else {
        
        // We've got a failure back from the EthernetMacDetect library while
        // attempting to obtain an embedded MAC address. There's no point
        // attempting re-connects at this point as we've choked with the fundamental
        // detail we need to establish an Ethernet connection (ie: the MAC address),
        // so we'll return immediately with an error status.
        
        return Error_AutoMACConfigurationFailed;
    }
    
}

ConnectNetworkServicesResult_t SpaceVanOS::connectNetworkServices(MqttIncomingCommandCallback commandCallback, MqttIncomingPresenceCallback presenceCallback) {

    // Attempt to obtain network settings and connect to LAN services
    // Assume that MAC can be obtained automatically by probing I2C EEPROM with embedded MAC address, and that
    // the local IP address will be obtained by DHCP. Also, set connectionAttempts to 1 only.
    
    return connectNetworkServices(commandCallback, presenceCallback, 1, 0);    
            
}

char* SpaceVanOS::getClientName(void) {

    return _clientName;   

}

char* SpaceVanOS::getClientVersion(void) {

    return _clientVersion;   

}

NetworkServicesInfo SpaceVanOS::getNetworkServicesInfo(void) {

    return _networkServices;

}

// PRIVATE FUNCTIONS

void SpaceVanOS::copyCharArray(char* dest, int length, char const* source) {

    for(uint8_t i=0; i < length; i++) {
        dest[i] = source[i];
    }

}

void SpaceVanOS::copyByteArray(byte* dest, int length, byte const* source) {
    
    for(uint8_t i=0; i < length; i++) {
        dest[i] = source[i];
    }

}

void SpaceVanOS::copyUint8Array(uint8_t* dest, int length, uint8_t const* source) {
    
    for(uint8_t i=0; i < length; i++) {
        dest[i] = source[i];
    }
    
}

char* SpaceVanOS::initCharArray(int length, char const initChar) {

    char* dest = new char[length];
    
    for(uint8_t i=0; i < length; i++) {
        dest[i] = initChar;
    }

    return dest;
    
}

byte* SpaceVanOS::initByteArray(int length, byte const initByte) {

    byte* dest = new byte[length];
    
    for(uint8_t i=0; i < length; i++) {
        dest[i] = initByte;
    }
    
    return dest;

}

uint8_t* SpaceVanOS::initUint8Array(int length, uint8_t const initUint8) {

    uint8_t* dest = new uint8_t[length];
    
    for(uint8_t i=0; i < length; i++) {
        dest[i] = initUint8;       
    }

    return dest;
    
}

uint8_t* SpaceVanOS::ipAddressToUint8Array(IPAddress ipAddressIn) {
 
    uint8_t octets[IPV4_ADDRESS_LENGTH];
    int x;

    for (x = 0; x < IPV4_ADDRESS_LENGTH; x++) {
        octets[x] = ipAddressIn[x];   
    }
      
    return octets;

} 

char* SpaceVanOS::ipAddressUint8ArrayToString(uint8_t const* ipAddressArrIn) {

    char result[((IPV4_ADDRESS_LENGTH * 3)+IPV4_ADDRESS_LENGTH)];
    
    snprintf(result, sizeof(result), "%d.%d.%d.%d", ipAddressArrIn[0], ipAddressArrIn[1], ipAddressArrIn[2], ipAddressArrIn[3] );
    
    return result;    
}
    
    
char* SpaceVanOS::ipAddressToString(IPAddress ipAddressIn) {
     
    return (ipAddressUint8ArrayToString(ipAddressToUint8Array(ipAddressIn)));

} 

char* SpaceVanOS::macAddressUint8ArrayToString(uint8_t const* macAddressIn) {
 
    char result[(MAC_ADDRESS_LENGTH * 3)];

    snprintf(result, sizeof(result), "%02X:%02X:%02X:%02X:%02X:%02X", macAddressIn[0], macAddressIn[1], macAddressIn[2], macAddressIn[3], macAddressIn[4], macAddressIn[5]);
    
    return result;
    
}