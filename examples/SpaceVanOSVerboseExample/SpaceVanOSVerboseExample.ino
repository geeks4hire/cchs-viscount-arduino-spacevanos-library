//
// ---------------------------------------------------------------------
//  SpaceVan-OS - To the darkest reaches of deep space - in a Caravan!
// ---------------------------------------------------------------------
//
//      Space Simulator Arduino Controller Node (Code Template)
//
// ---------------------------------------------------------------------
//
//       Created for Connected Community HackerSpace Melbourne
//            [http://www.hackmelbourne.org.au/] for the:
//        CCHS Viscountess Spaceship Simulator (in a caravan!)
//
// ---------------------------------------------------------------------
//
//   Author(s):  Ben Horan <benh@geeksforhire.com.au>
//               John Spencer <insert email here>
//
//   License:    Copyright (c) 2014, Ben Horan & John Spencer
//               Released under GNU Affero General Public License v3
//               [https://gnu.org/licenses/agpl.txt]
//
//  Platform:    This code, and its dependancy libraries have been
//               tested on the FreeTronics EtherMega rev.3 (which
//               includes the Microchip 24AA02E48 EEPROM with unique
//               MAC address). It uses several large libraries for
//               specialist networking and other communications tasks
//               which will not compile into 8/16/32kbit program memory
//               Arduino processors (the MEGA just gives us heaps of 
//               GPIO's and tonnes of memory - who could resist??).
//
//  Version History:    version 0.2 beta (Current Version)
//                          - Improved commenting
//                          - Added network connection retry logic
//                          - Added callback functions for incoming
//                            MQTT messages relevant to this client.
//                      version 0.1 beta 
//                          - Initial Release
//

// Debugging settings
#define DEBUG
#define DEBUG_SERIAL_BAUD 115200

// Specifies how many times to attempt to connect to the network 
// (and how many milliseconds to delay between each retry)
#define NETWORK_MAX_CONNECTION_RETRIES 10
#define NETWORK_RETRY_DELAY 30000

#include <debug.h>

#include <SpaceVanOS.h>

#include <Wire.h>
#include <SPI.h>
#include <Dhcp.h>
#include <Dns.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetUdp.h>
#include <EthernetMacDetect.h>

// Define client name and software version for this specific client node
const char* CLIENT_NAME = "pilot-controls1";
const char* CLIENT_SOFTWARE_VERSION = "0.2.0";

// Our SpaceVanOS object will handle all network communications and other general workload
SpaceVanOS spaceship;

// Callback function definitions for incoming MQTT messages to this client
void mqttIncomingCommand(char* commmandTopic, char* commandPayload);
void mqttIncomingPresence(char* clientName, MqttClientPresence_t presence);

//
// --------------------------------------------------------------------------------------------------------------------
//

// Client Initialisation

void setup() {
  
    // If we've defined DEBUG above, use helper macros to output information to Serial connection.
    // Comment out the above "#define DEBUG" line (but leave the "#include <debug.h>"!), and all
    // of the below debug actions will be removed, and not compiled into the resultant output.
    // (Recommended when releasing a production release of Arduino software as it will reduce the
    // amount of program & dynamic memory taken up by the 'no longer needed' debug code).

    DEBUG_SERIALCONN(DEBUG_SERIAL_BAUD);
    DEBUG_PRINTLN("--------------------------------------------------------------------------------------------------------------------------------------------");
    DEBUG_PRINT("                                                       SpaceVan Operating System v");
    DEBUG_PRINTLN(CLIENT_SOFTWARE_VERSION);
    DEBUG_PRINTLN("--------------------------------------------------------------------------------------------------------------------------------------------");
    DEBUG_PRINTLN("");
    DEBUG_PRINTLN("  -- Now initialising SpaceVanOS core systems..."); 
    DEBUG_PRINTLN("");
    
    // Initialise our SpaceVanOS object for use throughout sketch
    
    spaceship.begin(CLIENT_NAME, CLIENT_SOFTWARE_VERSION);
    
    // Attempt to auto-configure network connection and connect to relevant services on the game server.
    //
    // NOTE: The below method call will auto-detect MAC address from I2C EEPROM on board (assuming we're
    //       running on a Freetronics EtherMega rev.3+). There are other overloads of this 
    //       connectNetworkServices() method if you need to manually set a MAC address, client IP address,
    //       etc - check the SpaceVanOS library header file or library README for further information).

    DEBUG_PRINTLN("  -- Now initialising Network client connections..."); 
    DEBUG_PRINTLN("");
    
    ConnectNetworkServicesResult_t connectionResult;
       
    connectionResult = spaceship.connectNetworkServices(mqttIncomingCommand, mqttIncomingPresence, NETWORK_MAX_CONNECTION_RETRIES, NETWORK_RETRY_DELAY);
    
    if (connectionResult != Successful) {
            
        DEBUG_PRINTLN("      + ERROR initialising networking components / connecting to server!");
        
        switch(connectionResult) {
          
            case Error_AutoMACConfigurationFailed:
                DEBUG_PRINTLN("             Cause: Unable to detect MAC address from onboard I2C MAC EEPROM - are you using the wrong method call??");
                break;
    
            case Error_DHCPConfigurationFailed:
                DEBUG_PRINTLN("             Cause: Unable to obtain IP address from DHCP server - is the LAN cable connected / do you have a DHCP server??");
                break;
    
            case Error_MQTTServerConnectionFailed:
                DEBUG_PRINTLN("             Cause: Unable to connect to the MQTT messaging broker (messaging server).");
                break;
    
            case Error_WebServerConnectionFailed:
                DEBUG_PRINTLN("             Cause: Unable to connect to the API web server.");                
                break;    
    
          }

          // We've exhausted our retries and still failed to connect - time to prevent setup progression with infinite loop.
          
          DEBUG_PRINTLN("");            
          DEBUG_PRINTLN("      ---> FATAL - EXCEEDED MAXIMUM RETRIES TO CONNECT TO NETWORK - ABORTING INITIALISATION! ");
          DEBUG_PRINTLN("");
    
          while(1) {
              // loop infinitely - we can't proceed any further... 
          }                
              
    }
        
    // If we've gotten this far, then we successfully initialised all of out networking / connections - hooray!

    DEBUG_PRINTLN("      + Successfully initialised networking and server connections!");
    DEBUG_PRINTLN("");

    NetworkServicesInfo netInfo = spaceship.getNetworkServicesInfo();

    // DEBUG - Display the results stored in the SpaceVanOS NetworkServicesInfo object after the connectNetworkServices() method above has succeeded.
    //         Will also display the status for server connections etc (to be used as the functionality in the SpaceVanOS library continues to grow)... 
#ifdef DEBUG
    char tempbuf[18];
    sprintf(tempbuf, "%02X:%02X:%02X:%02X:%02X:%02X", netInfo.ethernetMacAddress[0], netInfo.ethernetMacAddress[1], netInfo.ethernetMacAddress[2], netInfo.ethernetMacAddress[3], netInfo.ethernetMacAddress[4], netInfo.ethernetMacAddress[5]);
    DEBUG_PRINT("             -> Ethernet MAC Address =       ");
    DEBUG_PRINTLN(tempbuf);   
    DEBUG_PRINT("             -> Local IP Address =           ");
    sprintf(tempbuf, "%d.%d.%d.%d", netInfo.localIP[0], netInfo.localIP[1], netInfo.localIP[2], netInfo.localIP[3]);
    DEBUG_PRINTLN(tempbuf);
    DEBUG_PRINT("             -> Subnet Mask =                ");
    sprintf(tempbuf, "%d.%d.%d.%d", netInfo.subnetMask[0], netInfo.subnetMask[1], netInfo.subnetMask[2], netInfo.subnetMask[3]);
    DEBUG_PRINTLN(tempbuf);
    DEBUG_PRINT("             -> Gateway IP Address =         ");
    sprintf(tempbuf, "%d.%d.%d.%d", netInfo.gatewayIP[0], netInfo.gatewayIP[1], netInfo.gatewayIP[2], netInfo.gatewayIP[3]);
    DEBUG_PRINTLN(tempbuf);
    DEBUG_PRINT("             -> DNS Server IP Address =      ");
    sprintf(tempbuf, "%d.%d.%d.%d", netInfo.dnsServerIP[0], netInfo.dnsServerIP[1], netInfo.dnsServerIP[2], netInfo.dnsServerIP[3]);
    DEBUG_PRINTLN(tempbuf);
    DEBUG_PRINT("             -> MQTT Server IP Address =     ");
    sprintf(tempbuf, "%d.%d.%d.%d", netInfo.mqttServerIP[0], netInfo.mqttServerIP[1], netInfo.mqttServerIP[2], netInfo.mqttServerIP[3]);
    DEBUG_PRINT(tempbuf);
    DEBUG_PRINT(" (Port: ");
    DEBUG_PRINT(netInfo.mqttServerPort);
    DEBUG_PRINTLN(")");
    DEBUG_PRINT("             -> Web Server IP Address =      ");
    sprintf(tempbuf, "%d.%d.%d.%d", netInfo.webServerIP[0], netInfo.webServerIP[1], netInfo.webServerIP[2], netInfo.webServerIP[3]);
    DEBUG_PRINT(tempbuf);
    DEBUG_PRINT(" (Port: ");
    DEBUG_PRINT(netInfo.webServerPort);
    DEBUG_PRINTLN(")");
    DEBUG_PRINTLN("");
    DEBUG_PRINT("             -> MAC Address Auto-Detected =  ");
    DEBUG_PRINTLN((netInfo.macAddressAutoDetected) ? "TRUE" : "FALSE");
    DEBUG_PRINT("             -> Local IP Obtained via DHCP = ");
    DEBUG_PRINTLN((netInfo.dhcpIpConfigured) ? "TRUE" : "FALSE");
    DEBUG_PRINT("             -> MQTT Msg Broker Connected  = ");
    DEBUG_PRINTLN((netInfo.mqttServerConnected) ? "TRUE" : "FALSE");
    DEBUG_PRINT("             -> Web API Server Connected  =  ");    
    DEBUG_PRINTLN((netInfo.webServerConnected) ? "TRUE" : "FALSE");
    DEBUG_PRINTLN("");
#endif
    // END DEBUG

}

//
// --------------------------------------------------------------------------------------------------------------------
//

void loop() {
  
    // record the number of milliseconds that our loop iteration takes (just in case this is needed)...
    unsigned long loopStart = millis();    
    
    // We need to call this .loop() method of the SpaceVanOS class every loop cycle. It ensures that scheduled 
    // events are fired, that incoming MQTT messages are handled, and generally makes sure all of the grunt-work
    // is taken off our hands. As this call may take a variable amount of time to complete, you should avoid
    // trying to precisely time the intervals of your main loop function iterations with arbitrary delay() values 
    // unless such delays are based on a calculation of each specific loop run duration time itself (example is
    // included here for demonstration).
    //
    // Use the .scheduleEvery() and .scheduleOscillate() methods within the SpaceVanOS class if you need to
    // ensure certain events happen at specific time intervals (ie: for regular GPIO polling/updates)...
    
    spaceship.loop();
    
    // calculate duration of this loop cycle in milliseconds
    unsigned long loopDuration = (millis() - loopStart);

}

//
// --------------------------------------------------------------------------------------------------------------------
//

// Callback function to handle incoming commands to this client on the "spaceship/<<selfClientName>>/commands/<<commandTopic>>/<<commandMessage>>" topics

void mqttIncomingCommand(char* commandTopic, char* commandMessage) {
  
    // Log debug info to serial port if debugging is enabled...
    
    DEBUG_PRINTLN("  -- mqttIncomingCommand callback called:");
    DEBUG_PRINTLN("");
    DEBUG_PRINT("      + commandTopic = ");
    DEBUG_PRINTLN(commandTopic);
    DEBUG_PRINT("      + commandMessage = ");
    DEBUG_PRINTLN(commandMessage);
    DEBUG_PRINTLN("");
    
}

// Callback function to handle updates on node presence on the "spaceship/presence/<<remoteClientName>>" topics

void mqttIncomingPresence(char* clientName, MqttClientPresence_t presence) {

    // Log debug info to serial port if debugging is enabled...
    
    DEBUG_PRINTLN("  -- mqttIncomingPresence callback called:");
    DEBUG_PRINTLN("");
    DEBUG_PRINT("      + clientName = ");
    DEBUG_PRINTLN(clientName);
    DEBUG_PRINT("      + presence = ");
    DEBUG_PRINTLN((int)presence);
    DEBUG_PRINTLN("");

}

