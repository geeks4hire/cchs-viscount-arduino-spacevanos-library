/*
 * Arduino Debug Helper Library - Library File (debug.h)
 *
 * Author:        Ben Horan <benh AT geeksforhire DOT com DOT au>
 * Version:       v1.0 (Released 20-Feb-2014)
 * Description:   The Arduino Debug Helper Library provides some coding shortcuts
 *                to allow you to embed debugging messages and information that will
 *                be sent back to the host computer (Terminal software, or the Arduino IDE
 *                Serial Monitor) whilst developing/testing programs you are developing.
 *                It relies on #define macros, and checks to see if you have defined "DEBUG"
 *                at the top of your code (ie: "#define DEBUG". If DEBUG is defined, then the
 *                helper macros in "debug.h" will be included in your compiled code uploaded to
 *                the Arduino. If you comment out, or remove the "#define DEBUG" line from your
 *                code, the compiler will automatically strip all of your debug messages during
 *                compilation, therefore reducing the amount of program memory taken up by your
 *                program, and also leaving your program running as it was designed, with no
 *                debug information being provided to the end user. Some of the macros (such as
 *                DEBUG_LPRINTLN and DEBUG_SPRINTLN include additional information about where
 *                the program execution is up to along with your message. Hopefully, more
 *                functionality will be added to future versions of this library, but for now,
 *                hopefully using these DEBUG_XXXXX macros in your code will save time and make
 *                it easy to switch from development to production mode on a project you're writing.
 *                
 * License:       This file is part of Arduino Debug Helper Library.
 *
 *                This library (and example code) is free software: you can redistribute it 
 *                and/or modify it under the terms of the GNU General Public License as 
 *                published by the Free Software Foundation, either version 3 of the License, 
 *                or (at your option) any later version.
 *
 *                This library (and accompanying files) is distributed in the hope that it will 
 *                be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                To obtain a full copy of the GNU General Public License v3, visit:    
 *                   ->   http://www.gnu.org/licenses/gpl.txt
 *
 *  Credits:      This library was made possible by the ideas shared at:  
 *                   ->   http://forum.arduino.cc/index.php/topic,46900.0.html
 *
 */ 

#ifndef DEBUG_H
#define DEBUG_H

#include <WProgram.h>

#ifdef DEBUG
#define DEBUG_SERIALCONN(baud) \
		Serial.begin(baud)
#define DEBUG_SERIAL   \
		Serial
#define DEBUG_PRINT(str) \
		Serial.print(str)
#define DEBUG_PRINTLN(str)  \
	  Serial.println(str)
#define DEBUG_LPRINTLN(str)    \
    Serial.print("[");    \
    Serial.print(millis());     \
    Serial.print("]: ");    \
    Serial.print(__FILE__);     \
    Serial.print(" - ");      \
    Serial.print(__PRETTY_FUNCTION__); \
    Serial.print(":   ");      \
    Serial.println(str)
#define DEBUG_SPRINTLN(str)    \
    Serial.print("[");    \
    Serial.print(millis());     \
    Serial.print("]:   ");    \
    Serial.println(str)
#define DEBUG_WRITE(val)    \
    Serial.write(val)
#else
#define DEBUG_SERIALCONN(baud)	;
#define DEBUG_SERIAL	//		;
#define DEBUG_PRINT			;				
#define DEBUG_PRINTLN				;			
#define DEBUG_LPRINTLN(str)		;	
#define DEBUG_SPRINTLN(str)			;
#define DEBUG_WRITE(val)    ;
#endif

#endif