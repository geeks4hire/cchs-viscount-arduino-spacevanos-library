/*
 * -----------------------------------------------------------------------
 *            EthernetMacDetect Library for Arduino v0.2 beta
 * -----------------------------------------------------------------------
 *
 *  PURPOSE: 
 *
 *    To automatically obtain an Ethernet MAC address stored within
 *    a specialised EEPROM chip embedded with a EUI-48 standards compliant,
 *    unique MAC address, in able to establish Ethernet client/service 
 *    functionality with your Arduino projects.
 *
 *  NOTES:
 *
 *    This library currently supports:
 *
 *           - Microchip 24AA02E48 I2C 2K Serial EEPROM with 
 *             48-bit Unique MAC Address
 *
 *    Tested on the following Arduino-compatible boards (with MAC chips): 
 *
 *           - Freetronics EtherMega microcontroller (rev.3)
 *           - Freetronics EtherTen microcontroller (rev.3)
 *              [Visit: www.freetronics.com for more info]
 *
 *     (If you wire a compatible I2C MAC-embedded EEPROM to any other
 *      Arduino board/Ethernet shield combo, it should also work fine)
 *
 *  RELEASE HISTORY:
 *
 *    Version 0.2 beta (current version)
 *           - Removed redundant private method for I2C MAC probing and
 *             moved content into getMacAddressFromEeprom().
 *           - Beefed up commenting to explain the steps taken to validate
 *             that the correct IC is being contacted and returning the
 *             expected MAC address format of data.
 * 
 *    Version 0.1 beta
 *           - Initial release
 *
 *  AUTHOR:
 *
 *    Ben Horan <benh@geeksforhire.com.au>
 *
 * -----------------------------------------------------------------------
 *
 *  THE LATEST VERSION OF THIS LIBRARY IS AVAILABLE FOR DOWNLOAD AT:
 *
 *      https://bitbucket.org/geeks4hire/arduino-ethernetmacdetect
 *
 * -----------------------------------------------------------------------
 * 
 *  Copyright 2014, Ben Horan <benh@geeksforhire.com.au>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */


#ifndef EthernetMacDetect_h
#define EthernetMacDetect_h

#include <stdlib.h>
#include <Arduino.h>
#include <Wire.h>

#ifndef MAC_EEPROM_I2C_ADDRESS
#define MAC_EEPROM_I2C_ADDRESS 0x50
#endif

#ifndef MAC_EEPROM_MEMORY_REGISTER
#define MAC_EEPROM_MEMORY_REGISTER 0xFA
#endif

#define MAC_ADDRESS_LENGTH 6

class EthernetMacDetectClass {

public:
  boolean getMacAddressFromEeprom(uint8_t *macAddr);
  boolean isMacAddressEmpty(uint8_t *macAddr);
  
};

extern EthernetMacDetectClass EthernetMacDetect;

#endif