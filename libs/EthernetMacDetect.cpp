/*
 * -----------------------------------------------------------------------
 *            EthernetMacDetect Library for Arduino v0.2 beta
 * -----------------------------------------------------------------------
 *
 *  PURPOSE: 
 *
 *    To automatically obtain an Ethernet MAC address stored within
 *    a specialised EEPROM chip embedded with a EUI-48 standards compliant,
 *    unique MAC address, in able to establish Ethernet client/service 
 *    functionality with your Arduino projects.
 *
 *  NOTES:
 *
 *    This library currently supports:
 *
 *           - Microchip 24AA02E48 I2C 2K Serial EEPROM with 
 *             48-bit Unique MAC Address
 *
 *    Tested on the following Arduino-compatible boards (with MAC chips): 
 *
 *           - Freetronics EtherMega microcontroller (rev.3)
 *           - Freetronics EtherTen microcontroller (rev.3)
 *              [Visit: www.freetronics.com for more info]
 *
 *     (If you wire a compatible I2C MAC-embedded EEPROM to any other
 *      Arduino board/Ethernet shield combo, it should also work fine)
 *
 *  RELEASE HISTORY:
 *
 *    Version 0.2 beta (current version)
 *           - Removed redundant private method for I2C MAC probing and
 *             moved content into getMacAddressFromEeprom().
 *           - Beefed up commenting to explain the steps taken to validate
 *             that the correct IC is being contacted and returning the
 *             expected MAC address format of data.
 * 
 *    Version 0.1 beta
 *           - Initial release
 *
 *  AUTHOR:
 *
 *    Ben Horan <benh@geeksforhire.com.au>
 *
 * -----------------------------------------------------------------------
 *
 *  THE LATEST VERSION OF THIS LIBRARY IS AVAILABLE FOR DOWNLOAD AT:
 *
 *      https://bitbucket.org/geeks4hire/arduino-ethernetmacdetect
 *
 * -----------------------------------------------------------------------
 * 
 *  Copyright 2014, Ben Horan <benh@geeksforhire.com.au>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

 
#include <Wire.h>
#include "EthernetMacDetect.h"


boolean EthernetMacDetectClass::getMacAddressFromEeprom(uint8_t *macAddr) {
  int i2cEepromAcknowledge;
  int currentByte = 0;
  
  Wire.begin();
  Wire.beginTransmission(MAC_EEPROM_I2C_ADDRESS);
  Wire.write(MAC_EEPROM_MEMORY_REGISTER); 
  
  // Check the return value from endTransmission(). A non-zero result
  // indicates either a failure to recieve an ACKnowledgement from the
  // EEPROM chip, or another problem relating to I2C communications.
  // This will be our first clue that perhaps we're probing a board
  // that doesn't contain the intended IC (or we're probing the wrong
  // I2C address...    
  i2cEepromAcknowledge = Wire.endTransmission(false);  
  if (i2cEepromAcknowledge == 0) {
      // Ask the I2C EEPROM for 6 bytes of data for the stored unique
      // MAC address stored on it.
      Wire.requestFrom(MAC_EEPROM_I2C_ADDRESS, MAC_ADDRESS_LENGTH);      

      while(!Wire.available())
      {
        // Wait until we've got some data back from the chip
      }
      
      while((Wire.available()) && (currentByte < MAC_ADDRESS_LENGTH)) { 
        byte v;
        v = Wire.read();    // receive a byte as character
        macAddr[currentByte] = (uint8_t)v;
        currentByte ++;
      }
      
      // Check that we got the requested number of bytes back from our I2C slave
      if (currentByte == MAC_ADDRESS_LENGTH) {
          
          // According to the datasheet [http://ww1.microchip.com/downloads/en/DeviceDoc/22124D.pdf]
          // the Microchip 24AA125E48 MAC Eeprom has a standard (fixed) manufacturer ID as the first 
          // 3 bytes of all MAC addresses it provides (00:04:A3). If we can confirm that the data  
          // that was returned contains these three values, we know we've read valid info from the
          // right I2C slave...
          
          if ((macAddr[0] == 0x00) && (macAddr[1] == 0x04) && (macAddr[2] == 0xA3)) {
            return true;
          }
      }
  }
  
  // If we either failed to get a valid ACK from the chip we were talking to, didn't get the desired number of bytes
  // for a valid MAC address, or if the MAC data failed the check for compliance with the Microchip datasheet for 
  // the fixed manufacturer ID bytes, then re-initialize our global macAddr variable to a blank 
  // (00:00:00:00:00:00) address and return false to indicate failure to the user.

  for (int i=0; i < MAC_ADDRESS_LENGTH; i++) {
    macAddr[i] = 0x00;
  }
  return false;    
}

boolean EthernetMacDetectClass::isMacAddressEmpty(uint8_t *macAddr) {
    
  boolean result = true;
  
  for (int i=0; i < MAC_ADDRESS_LENGTH; i++) {
    if (macAddr[i] != 0x00) {
      result = false;   
    }
  }
   
  return result;
  
}

EthernetMacDetectClass EthernetMacDetect;